from graphics import *
import json
class Graphs:
    def __init__(self, x, y, win):
        self.charts = []
        self.labels = []
        self.boxes = []
        self.x = x
        self.y = y
        self.win = win

        self.labels.append(Text(Point(self.x+470, self.y+22), "Silver Chart"))
        self.labels.append(Text(Point(self.x+1430, self.y+22), "Gold Chart"))

        for x in self.labels:
            x.setTextColor('white')
            x.setSize(36)
            x.setStyle("bold")

        self.boxes.append(Rectangle(Point(self.x, self.y), Point(self.x+940, self.y+400)))
        self.boxes.append(Rectangle(Point(self.x+960, self.y), Point(self.x+1900, self.y+400)))
        self.boxes[0].setFill(color_rgb(0, 0, 0))
        self.boxes[1].setFill(color_rgb(0, 0, 0))

        self.boxes.append(Rectangle(Point(self.x+110, self.y), Point(self.x+940, self.y+41)))
        self.boxes.append(Rectangle(Point(self.x+1070, self.y), Point(self.x+1900, self.y+47)))
        self.boxes[2].setFill(color_rgb(0, 0, 0))
        self.boxes[3].setFill(color_rgb(0, 0, 0))


    def draw(self):
        self.charts.append(Image(Point(self.x+470, self.y+200), "datFiles/silver.gif"))
        self.charts.append(Image(Point(self.x+1430, self.y+200), "datFiles/gold.gif"))

        self.boxes[0].draw(self.win)
        self.boxes[1].draw(self.win)

        for x in self.charts:
            x.draw(self.win)

        self.boxes[2].draw(self.win)
        self.boxes[3].draw(self.win)

        for x in self.labels:
            x.draw(self.win)

        return()

    def clear(self):
        for x in self.charts:
            x.undraw()

        for x in self.boxes:
            x.undraw()

        for x in self.labels:
            x.undraw()

        del self.charts[:]

        return()
