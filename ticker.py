from graphics import *
import json
class Ticker:
    def __init__(self, x, y, win):
        self.boxes = []
        self.label = []
        self.ask = []
        self.bid = []
        self.high = []
        self.low = []
        self.change = []
        self.percent = []
        self.x = x
        self.y = y
        self.win = win

        self.boxes.append(Rectangle(Point(self.x+0,self.y+0), Point(self.x+1775,self.y+60)))
        self.boxes.append(Rectangle(Point(self.x+0,self.y+60), Point(self.x+1775,self.y+120)))
        self.boxes.append(Rectangle(Point(self.x+0,self.y+120), Point(self.x+1775,self.y+180)))
        self.boxes.append(Rectangle(Point(self.x+0,self.y+180), Point(self.x+1775,self.y+240)))
        self.boxes.append(Rectangle(Point(self.x+0,self.y+240), Point(self.x+1775,self.y+300)))
        self.boxes.append(Line(Point(self.x+275, self.y+60), Point(self.x+275, self.y+300)))
        self.boxes.append(Line(Point(self.x+525, self.y+60), Point(self.x+525, self.y+300)))
        self.boxes.append(Line(Point(self.x+775, self.y+60), Point(self.x+775, self.y+300)))
        self.boxes.append(Line(Point(self.x+1025, self.y+60), Point(self.x+1025, self.y+300)))
        self.boxes.append(Line(Point(self.x+1275, self.y+60), Point(self.x+1275, self.y+300)))
        self.boxes.append(Line(Point(self.x+1525, self.y+60), Point(self.x+1525, self.y+300)))

        self.boxes.append(Line(Point(self.x+275, self.y+0), Point(self.x+275, self.y+60)))
        self.boxes.append(Line(Point(self.x+525, self.y+0), Point(self.x+525, self.y+60)))
        self.boxes.append(Line(Point(self.x+775, self.y+0), Point(self.x+775, self.y+60)))
        self.boxes.append(Line(Point(self.x+1275, self.y+0), Point(self.x+1275, self.y+60)))
        self.boxes.append(Line(Point(self.x+1525, self.y+0), Point(self.x+1525, self.y+60)))




        self.label.append(Text(Point(self.x+125,self.y+30), "Metals"))
        self.label.append(Text(Point(self.x+125,self.y+90), "Silver"))
        self.label.append(Text(Point(self.x+125,self.y+150), "Gold"))
        self.label.append(Text(Point(self.x+125,self.y+210), "Platinum"))
        self.label.append(Text(Point(self.x+125,self.y+270), "Palladium"))

        self.label.append(Text(Point(self.x+400, self.y+30), "High Bid"))
        self.label.append(Text(Point(self.x+650, self.y+30), "Low Ask"))
        self.label.append(Text(Point(self.x+1025, self.y+30), "24h Change"))
        self.label.append(Text(Point(self.x+1400, self.y+30), "24h High"))
        self.label.append(Text(Point(self.x+1650, self.y+30), "24h Low"))

        self.boxes[0].setFill(color_rgb(0, 0, 0))
        self.boxes[1].setFill(color_rgb(225, 225, 225))
        self.boxes[2].setFill(color_rgb(200, 200, 200))
        self.boxes[3].setFill(color_rgb(225, 225, 225))
        self.boxes[4].setFill(color_rgb(200, 200, 200))

        self.boxes[11].setFill(color_rgb(200, 200, 200))
        self.boxes[12].setFill(color_rgb(200, 200, 200))
        self.boxes[13].setFill(color_rgb(200, 200, 200))
        self.boxes[14].setFill(color_rgb(200, 200, 200))
        self.boxes[15].setFill(color_rgb(200, 200, 200))

        for x in self.label:
            x.setTextColor('black')
            x.setSize(36)
            x.setStyle("bold")
        self.label[0].setTextColor('white')
        self.label[5].setTextColor('white')
        self.label[6].setTextColor('white')
        self.label[7].setTextColor('white')
        self.label[8].setTextColor('white')
        self.label[9].setTextColor('white')

        for x in self.boxes:
            x.setWidth(3)
            x.draw(self.win)

        for x in self.label:
            x.draw(self.win)



    def draw(self):
        with open("datFiles/ticker.dat") as json_file:
                ticker_data = json.load(json_file)

        self.bid.append(Text(Point(self.x+400, self.y+90), ticker_data["silver"]["bid"]))
        self.bid.append(Text(Point(self.x+400, self.y+150), ticker_data["gold"]["bid"]))
        self.bid.append(Text(Point(self.x+400, self.y+210), ticker_data["platinum"]["bid"]))
        self.bid.append(Text(Point(self.x+400, self.y+270), ticker_data["palladium"]["bid"]))

        self.ask.append(Text(Point(self.x+650, self.y+90), ticker_data["silver"]["ask"]))
        self.ask.append(Text(Point(self.x+650, self.y+150), ticker_data["gold"]["ask"]))
        self.ask.append(Text(Point(self.x+650, self.y+210), ticker_data["platinum"]["ask"]))
        self.ask.append(Text(Point(self.x+650, self.y+270), ticker_data["palladium"]["ask"]))

        self.change.append(Text(Point(self.x+900, self.y+90), ticker_data["silver"]["change"]))
        self.change.append(Text(Point(self.x+900, self.y+150), ticker_data["gold"]["change"]))
        self.change.append(Text(Point(self.x+900, self.y+210), ticker_data["platinum"]["change"]))
        self.change.append(Text(Point(self.x+900, self.y+270), ticker_data["palladium"]["change"]))

        self.percent.append(Text(Point(self.x+1150, self.y+90), ticker_data["silver"]["chang_percent"]))
        self.percent.append(Text(Point(self.x+1150, self.y+150), ticker_data["gold"]["chang_percent"]))
        self.percent.append(Text(Point(self.x+1150, self.y+210), ticker_data["platinum"]["chang_percent"]))
        self.percent.append(Text(Point(self.x+1150, self.y+270), ticker_data["palladium"]["chang_percent"]))

        self.high.append(Text(Point(self.x+1400, self.y+90), ticker_data["silver"]["high"]))
        self.high.append(Text(Point(self.x+1400, self.y+150), ticker_data["gold"]["high"]))
        self.high.append(Text(Point(self.x+1400, self.y+210), ticker_data["platinum"]["high"]))
        self.high.append(Text(Point(self.x+1400, self.y+270), ticker_data["palladium"]["high"]))

        self.low.append(Text(Point(self.x+1650, self.y+90), ticker_data["silver"]["low"]))
        self.low.append(Text(Point(self.x+1650, self.y+150), ticker_data["gold"]["low"]))
        self.low.append(Text(Point(self.x+1650, self.y+210), ticker_data["platinum"]["low"]))
        self.low.append(Text(Point(self.x+1650, self.y+270), ticker_data["palladium"]["low"]))

        for x in self.ask:
            x.setTextColor('black')
            x.setStyle("bold")
            x.setSize(36)
            x.draw(self.win)

        for x in self.bid:
            x.setTextColor('black')
            x.setStyle("bold")
            x.setSize(36)
            x.draw(self.win)

        for x in self.high:
            x.setTextColor('black')
            x.setStyle("bold")
            x.setSize(36)
            x.draw(self.win)

        for x in self.low:
            x.setTextColor('black')
            x.setStyle("bold")
            x.setSize(36)
            x.draw(self.win)

        for x in self.change:
            if "-" in x.getText():
                x.setTextColor(color_rgb(150, 0, 0))
            elif "+" in x.getText():
                x.setTextColor(color_rgb(20, 80, 0))
            else:
                x.setTextColor(color_rgb(130, 130, 0))
            x.setSize(36)
            x.setStyle("bold")
            x.draw(self.win)

        for x in self.percent:
            if "-" in x.getText():
                x.setTextColor(color_rgb(150, 0, 0))
            elif "+" in x.getText():
                x.setTextColor(color_rgb(20, 80, 0))
            else:
                x.setTextColor(color_rgb(130, 130, 0))
            x.setSize(36)
            x.setStyle("bold")
            x.draw(self.win)

        return()

    def clear(self):

        for x in self.ask:
            x.undraw()

        for x in self.bid:
            x.undraw()

        for x in self.high:
            x.undraw()

        for x in self.low:
            x.undraw()

        for x in self.change:
            x.undraw()

        for x in self.percent:
            x.undraw()

        del self.ask[:]
        del self.bid[:]
        del self.high[:]
        del self.low[:]
        del self.change[:]
        del self.percent[:]

        return()
