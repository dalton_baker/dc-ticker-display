#!/usr/bin/env python
from graphics import *
from ticker import *
from marketStatus import *
from ratio import *
from graphs import *
from usd import *
from djia import *
import subprocess
import time


def tryDraw(obj, count):
    count+=1

    try:
       obj.draw()
    except:
       if count > 30:
           return
       time.sleep(1)
       tryDraw(obj, count)

def main():
    win = GraphWin("Dakota Coin Metals Market (Data From Kitco)", 1920, 1100)
    win.setBackground(color_rgb(0, 0, 80))

    ticker = Ticker(72, 280, win)
    market = MarketStatus(72, 10, win)
    ratio = Ratio(72, 145, win)
    usd = USD(1497, 10, win)
    djia = DJIA(1367, 145, win)
    graphs = Graphs(10, 600, win)

    Image(Point(960, 135), "assets/dcLogo.gif").draw(win)

    try:
        while True:
            ticker.clear()
            market.clear()
            ratio.clear()
            graphs.clear()
            usd.clear()
            djia.clear()

            tryDraw(ticker, 0)
            tryDraw(market, 0)
            tryDraw(ratio, 0)
            tryDraw(usd, 0)
            tryDraw(djia, 0)
            tryDraw(graphs, 0)

            errorLog = open("ErrorLog.dat", "a")
            errorLog.write("Got out of the try func\n")
            errorLog.close()

            for x in range(60):
                time.sleep(1)
                if win.checkKey() == "C":
                    win.close()
                    exit(0)
    except Exception as e:
        errorLog = open("ErrorLog.dat", "a")
        errorLog.write(str(e) + "\n")
        errorLog.close()


main()
