from graphics import *
import json
class DJIA:
    def __init__(self, x, y, win):
        self.boxes = []
        self.label = []
        self.data = []
        self.x = x
        self.y = y
        self.win = win

        self.boxes.append(Rectangle(Point(self.x+0,self.y+0), Point(self.x+480,self.y+60)))
        self.boxes.append(Rectangle(Point(self.x+0,self.y+60), Point(self.x+480,self.y+120)))

        self.label.append(Text(Point(self.x+250,self.y+30), "Dow Jones"))

        self.boxes[0].setFill(color_rgb(0, 0, 0))
        self.boxes[1].setFill(color_rgb(200, 200, 200))

        for x in self.label:
            x.setTextColor('white')
            x.setSize(36)
            x.setStyle("bold")

        for x in self.boxes:
            x.setWidth(3)
            x.draw(self.win)

        for x in self.label:
            x.draw(self.win)


    def draw(self):
        with open("datFiles/ticker.dat") as json_file:
                ticker_data = json.load(json_file)

        self.data.append(Text(Point(self.x+140, self.y+90), ticker_data["djiaIndex"]["price"]))
        self.data.append(Text(Point(self.x+370, self.y+90), ticker_data["djiaIndex"]["change"]))
        #self.data.append(Text(Point(self.x+400, self.y+90), ticker_data["djiaIndex"]["chang_percent"]))

        self.data[0].setTextColor('black')

        if "-" in self.data[1].getText():
            self.data[1].setTextColor(color_rgb(150, 0, 0))
        elif "+" in self.data[1].getText():
            self.data[1].setTextColor(color_rgb(20, 80, 0))
        else:
            self.data[1].setTextColor(color_rgb(130, 130, 0))

        for x in self.data:
            x.setSize(36)
            x.setStyle("bold")
            x.draw(self.win)

        return()

    def clear(self):
        for x in self.data:
            x.undraw()

        del self.data[:]

        return()
